<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Dashboard\ApplicationsController;
use App\Http\Controllers\Dashboard\AttributeColumnsController;
use App\Http\Controllers\Dashboard\AttributeRowsController;
use App\Http\Controllers\Dashboard\AttributesController;
use App\Http\Controllers\Dashboard\AttributeValueController;
use App\Http\Controllers\Dashboard\CategoriesController;
use App\Http\Controllers\Dashboard\DiscountsController;
use App\Http\Controllers\Dashboard\FiltersController;
use App\Http\Controllers\Dashboard\IndexController;
use App\Http\Controllers\Dashboard\InstallationsController;
use App\Http\Controllers\Dashboard\MaterialsController;
use App\Http\Controllers\Dashboard\OrdersController;
use App\Http\Controllers\Dashboard\PaymentsController;
use App\Http\Controllers\Dashboard\PersonalizationsController;
use App\Http\Controllers\Dashboard\PersonalizationGroupController;
use App\Http\Controllers\Dashboard\PersonalizationItemController;
use App\Http\Controllers\Dashboard\PhotosController;
use App\Http\Controllers\Dashboard\ProductsController;
use App\Http\Controllers\Dashboard\ProfileController;
use App\Http\Controllers\Dashboard\PagesController;
use App\Http\Controllers\Dashboard\RegistersController;
use App\Http\Controllers\Dashboard\ReviewsController;
use App\Http\Controllers\Dashboard\SettingsController;
use App\Http\Controllers\Dashboard\ShippingsController;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'dashboard.', 'middleware' => [ 'app.dashboard', 'auth', 'verified' ], 'prefix' => 'dashboard'], function () {

    Route::get('/', [IndexController::class, 'index'])->name('index');

    Route::resources([
        'categories' => CategoriesController::class,
        'products' => ProductsController::class,
        'filters' => FiltersController::class,
        'materials' => MaterialsController::class,
        'shippings' => ShippingsController::class,
        'discounts' => DiscountsController::class,
        'personalizations' => PersonalizationsController::class,
        'registers' => RegistersController::class,
        'payments' => PaymentsController::class,
        'orders' => OrdersController::class,
        'photos' => PhotosController::class,
    ], ['except' => [ 'show' ]]);

    Route::resources([
        'installations' => InstallationsController::class,
        'attributes' => AttributesController::class,
        'attribute-columns' => AttributeColumnsController::class,
        'attribute-rows' => AttributeRowsController::class,
        'attribute-value' => AttributeValueController::class,
        'personalization-group' => PersonalizationGroupController::class,
        'personalization-item' => PersonalizationItemController::class,
    ], ['only' => [ 'store', 'update', 'destroy' ]]);

    Route::post('filters/store-item', [FiltersController::class, 'storeItem'])->name('filters.store-item');
    Route::put('filters/update-item/{item_id}', [FiltersController::class, 'updateItem'])->name('filters.update-item');
    Route::delete('filters/destroy-item/{item_id}', [FiltersController::class, 'destroyItem'])->name('filters.destroy-item');

    Route::group(['as' => 'settings.', 'prefix' => 'settings'], function () {
        Route::get('pages', [PagesController::class, 'index'])->name('pages.index');
        Route::get('pages/{slug}', [PagesController::class, 'edit'])->name('pages.edit');
        Route::put('pages/{slug}', [PagesController::class, 'update'])->name('pages.update');

        Route::get('/', [SettingsController::class, 'index'])->name('general.index');
        Route::put('update', [SettingsController::class, 'update'])->name('general.update');
    });

    Route::group(['as' => 'profile.', 'prefix' => 'profile'], function () {
        Route::get('/', [ProfileController::class, 'index'])->name('index');
        Route::put('update', [ProfileController::class, 'update'])->name('update');
        Route::post('update-password', [ProfileController::class, 'updatePassword'])->name('update-password');
    });

    Route::get('applications', [ApplicationsController::class, 'index'])->name('applications.index');
    Route::get('reviews', [ReviewsController::class, 'index'])->name('reviews.index');
});

Route::group(['as' => 'api.', 'prefix' => 'api', 'middleware' => [ 'auth', 'verified' ]], function () {
    Route::post('image-save', [ApiController::class, 'saveImage'])->name('image.save');
    Route::post('image-destroy', [ApiController::class, 'destroyFile'])->name('image.destroy');
});
