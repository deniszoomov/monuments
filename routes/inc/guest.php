<?php

use App\Http\Controllers\Guest\AboutUsController;
use App\Http\Controllers\Guest\ApplicationController;
use App\Http\Controllers\Guest\BasketController;
use App\Http\Controllers\Guest\CatalogController;
use App\Http\Controllers\Guest\ContactsController;
use App\Http\Controllers\Guest\DeliveryController;
use App\Http\Controllers\Guest\DiscountsController;
use App\Http\Controllers\Guest\FavoriteController;
use App\Http\Controllers\Guest\GuaranteeController;
use App\Http\Controllers\Guest\InstallationController;
use App\Http\Controllers\Guest\OrderController;
use App\Http\Controllers\Guest\OurWorkController;
use App\Http\Controllers\Guest\PaymentController;
use App\Http\Controllers\Guest\PolicyController;
use App\Http\Controllers\Guest\ProductsController;
use App\Http\Controllers\Guest\ReviewsController;
use App\Http\Controllers\Guest\SearchController;
use App\Http\Controllers\Guest\TermsController;
use App\Http\Controllers\Guest\ViewController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::middleware(['app.guest', 'session.key'])->group(function () {

    Route::get('/', [CatalogController::class, 'welcome'])->name('catalog');

    Route::get('favorites', [FavoriteController::class, 'index'])->name('favorites');
    Route::post('favorites/{product_id}/store', [FavoriteController::class, 'store'])->name('favorites.store');
    Route::delete('favorites/{product_id}/destroy', [FavoriteController::class, 'destroy'])->name('favorites.destroy');
    Route::delete('favorites/destroy/all', [FavoriteController::class, 'destroyAll'])->name('favorites.destroy-all');

    Route::get('basket', [BasketController::class, 'index'])->name('basket');
    Route::post('basket/{product_id}/store', [BasketController::class, 'store'])->name('basket.store');
    Route::delete('basket/{item_id}/destroy', [BasketController::class, 'destroy'])->name('basket.destroy');
    Route::post('basket/{item_id}/add-count', [BasketController::class, 'addCount'])->name('basket.add-count');
    Route::post('basket/{item_id}/rem-count', [BasketController::class, 'remCount'])->name('basket.rem-count');

    Route::post('applications/store', [ApplicationController::class, 'store'])->name('applications.store');

    Route::post('order/store', [OrderController::class, 'store'])->name('order.store');
    Route::get('order/successful', [OrderController::class, 'successful'])->name('order.successful');


    Route::get('about-us', [AboutUsController::class, 'index'])->name('about-us');
    Route::get('terms', [TermsController::class, 'index'])->name('terms');
    Route::get('installation', [InstallationController::class, 'index'])->name('installation');
    Route::get('delivery', [DeliveryController::class, 'index'])->name('delivery');
    Route::get('guarantee', [GuaranteeController::class, 'index'])->name('guarantee');
    Route::get('payment', [PaymentController::class, 'index'])->name('payment');
    Route::get('discounts', [DiscountsController::class, 'index'])->name('discounts');

    Route::get('reviews',[ReviewsController::class, 'index'])->name('reviews');
    Route::post('reviews', [ReviewsController::class, 'store'])->name('reviews.store');
    Route::get('contacts', [ContactsController::class, 'index'])->name('contacts');

    Route::get('our-work', [OurWorkController::class, 'index'])->name('ourwork');
    Route::get('policy', [PolicyController::class, 'index'])->name('policy');

    Route::get('/search', [SearchController::class, 'index'])->name('search');
    Route::get('/viewed', [ViewController::class, 'index'])->name('viewed');


    Route::get('/{category_slug}', [CatalogController::class, 'category'])->name('catalog.category');
    Route::get('/{category_slug}/{children_slug}', [CatalogController::class, 'categoryChildren'])->name('catalog.category-children');
    Route::get('/{category_slug}/{children_slug}/{product_slug}', [ProductsController::class, 'show'])->name('product');
});
