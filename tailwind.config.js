const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Open Sans', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'primary': {
                    light: '#db444a',
                    DEFAULT: '#d8373e',
                    dark: '#d2272f',
                },
                'footer': {
                    DEFAULT: 'rgba(234,234,234,0.65)',
                    dark: '#e1e1e1',
                },
                'secondary': {
                    light: '#a5a5a5',
                    DEFAULT: '#545454',
                    dark: '#3b3b3b',
                }
            }
        },
    },
    placeholderColor: {
        'primary': '#d8373e'
    },

    plugins: [require('@tailwindcss/forms')],
};
