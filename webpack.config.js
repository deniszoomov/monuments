const path = require('path');

module.exports = {
    module: {
        rules: [{
            test: /\.mjs$/,
            resolve: {fullySpecified: false},
            include: /node_modules/,
            type: "javascript/auto"
        },{
            test: /\.jsx?$/,
            exclude: /node_modules(?!\/foundation-sites)|bower_components/,
            use: [
                {
                    loader: 'babel-loader',
                    options: Config.babel()
                }
            ]
        }]
    },
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
        },
    }
};
