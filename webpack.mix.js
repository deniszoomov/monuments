const mix = require('laravel-mix');
require('laravel-mix-merge-manifest')

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .postCss('resources/css/guest/app.css', 'public/css/guest.css', [
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
    ])
    .postCss('resources/css/dashboard/app.css', 'public/css/dashboard.css', [
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
    ])
    .copy('resources/construkt', 'public/construkt')
    .webpackConfig(require('./webpack.config'))
    .mergeManifest();

// if (mix.inProduction()) {
//     mix.version();
// }
