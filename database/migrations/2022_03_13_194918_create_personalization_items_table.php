<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalizationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personalization_items', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('personalization_group_id')->unsigned();
            $table->foreign('personalization_group_id')->references('id')->on('personalization_groups')->onDelete('cascade');

            $table->string('name');
            $table->integer('sum')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personalization_items');
    }
}
