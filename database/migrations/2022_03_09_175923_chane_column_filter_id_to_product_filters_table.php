<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChaneColumnFilterIdToProductFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_filters', function (Blueprint $table) {
            $table->dropForeign(['filter_id']);
            $table->dropColumn('filter_id');

            $table->bigInteger('filter_items_id')->after('product_id')->unsigned();
            $table->foreign('filter_items_id')->references('id')->on('filter_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_filters', function (Blueprint $table) {
            $table->bigInteger('filter_id')->unsigned();
            $table->foreign('filter_id')->references('id')->on('filters')->onDelete('cascade');

            $table->dropForeign(['filter_items_id']);
            $table->dropColumn('filter_items_id');
        });
    }
}
