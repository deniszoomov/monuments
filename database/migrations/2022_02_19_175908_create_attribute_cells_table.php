<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_cells', function (Blueprint $table) {
            $table->id();
            $table->string('value')->nullable();

            $table->bigInteger('attribute_column_id')->unsigned();
            $table->foreign('attribute_column_id')->references('id')->on('attribute_columns')->onDelete('cascade');

            $table->bigInteger('attribute_row_id')->unsigned();
            $table->foreign('attribute_row_id')->references('id')->on('attribute_rows')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_cells');
    }
}
