<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'sort',
        'slug',
        'category_id'
    ];

    public function items()
    {
        return $this->hasMany(FilterItems::class)->orderBy('sort');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
