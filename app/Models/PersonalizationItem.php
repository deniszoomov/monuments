<?php

namespace App\Models;

use App\Traits\NumberFormatTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalizationItem extends Model
{
    use HasFactory, NumberFormatTrait;

    protected $fillable = [
        'personalization_group_id',
        'name',
        'sum'
    ];

    protected $appends = [
        'full_name',
        'price_string'
    ];

    public function getPriceStringAttribute()
    {
        return self::maney($this->sum ?? 0);
    }

    public function getFullNameAttribute()
    {
        return $this->name.' - '.self::maney($this->sum ?? 0);
    }
}
