<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhotoFilter extends Model
{
    use HasFactory;

    protected $fillable = [
        'photo_id',
        'filter_items_id',
    ];

    public static function updatePhotoItems($photo_id, $filters)
    {
        $currentFilters = self::wherePhotoId($photo_id)->get()->keyBy('filter_items_id');

        foreach ($filters as $filterId) {
            if (!$currentFilters->firstWhere('filter_items_id', $filterId)) {
                self::create([
                    'photo_id' => $photo_id,
                    'filter_items_id' => $filterId,
                ]);
            }
            $currentFilters->forget($filterId);
        }

        foreach ($currentFilters as $currentFilter) {
            $currentFilter->delete();
        }

        return true;
    }

}
