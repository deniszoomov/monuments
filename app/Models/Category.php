<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'sort',
        'parent_id',
        'title',
        'about',
        'shipping',
        'personalizations',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'img',
        'filter_show',
        'general',
    ];

    protected $casts = [
        'general' => 'boolean',
        'filter_show' => 'boolean'
    ];

    protected $appends = [
        'img_path',
        'has_filter_show',
    ];

    public function getImgPathAttribute()
    {
        return $this->img ? url($this->img) : null;
    }

    public function getHasFilterShowAttribute()
    {
        return !$this->parent_id;
    }

    public function childrens()
    {
        return $this->hasMany(self::class, 'parent_id')->orderBy('sort');
    }

    public function parent()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class)->orderBy('created_at', 'desc');
    }

    public function filters()
    {
        return $this->hasMany(Filter::class)->orderBy('sort');
    }

    public static function getNameListWithChildren($except_id = null)
    {
        $query = self::with('parent');
        if ($except_id) {
            $query->where('id', '<>', $except_id)
                ->where(function ($query) use ($except_id) {
                    $query->where('parent_id', '<>', $except_id);
                    $query->orWhere('parent_id', null);
                });
        }

        return $query->get()->map(function ($category) {
            $name = $category->name;
            if ($category->parent) {
                $name = $category->parent->name . ' > ' . $name;
            }
            return [
                'id' => $category->id,
                'name' => $name,
                'sort' => $category->sort
            ];
        });
    }

    public static function getNameListOnlyChildren($except_id = null)
    {
        $query = self::with('parent')->orderBy('sort')->whereHas('parent');
        if ($except_id) {
            $query->where('id', '<>', $except_id)
                ->where(function ($query) use ($except_id) {
                    $query->where('parent_id', '<>', $except_id);
                    $query->orWhere('parent_id', null);
                });
        }

        return $query->get()->map(function ($category) {
            $name = $category->name;
            if ($category->parent) {
                $name = $category->parent->name . ' > ' . $name;
            }
            return [
                'id' => $category->id,
                'name' => $name,
                'sort' => $category->sort
            ];
        });
    }

    public static function getCategoriesForList()
    {
        $query = self::with('parent')->orderBy('sort')->whereHas('parent')
            ->select(['id', 'name', 'slug', 'img', 'parent_id']);

        return $query->get()->map(function ($category) {
            return [
                'id' => $category->id,
                'name' => $category->name,
                'sort' => $category->sort,
                'img' => $category->img,
                'img_path' => $category->img_path,
                'href' => route('catalog.category-children', [$category->parent->slug, $category->slug])
            ];
        });
    }
}
