<?php

namespace App\Models;

use App\Repositories\FavoriteRepository;
use App\Traits\NumberFormatTrait;
use App\Traits\PaginatorTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, NumberFormatTrait, PaginatorTrait;

    protected $fillable = [
        'name',
        'art',
        'slug',
        'img',
        'additional_img',
        'about',
        'text',
        'status',
        'category_id',
        'has_shipping',
        'has_material',
        'has_personalizations',
        'has_installation',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'minimal_price',
    ];

    protected $appends = [
        'img_path',
        'is_favorite',
        'minimal_price_string'
    ];

    public function getMinimalPriceStringAttribute()
    {
        return self::maney($this->minimal_price ?? 0);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function installations()
    {
        return $this->hasMany(Installation::class)->orderBy('sort');
    }

    public function filterItems()
    {
        return $this->belongsToMany(FilterItems::class, ProductFilter::class);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class)->orderBy('sort');
    }

    public function generalAttribute()
    {
        return $this->hasOne(Attribute::class)->orderBy('sort');
    }

    public function getImgPathAttribute()
    {
        return $this->img ? url($this->img) : null;
    }

    public function getIsFavoriteAttribute()
    {
        return (new FavoriteRepository())->isSelected($this->id);
    }

    public function views()
    {
        return $this->hasMany(View::class);
    }

    /**
     * Формирует список продуктов
     * для каталога
     *
     * @param $category_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public static function getForCatalog($category_id = null, string $search = null, $filters = [], $limit = null, $is_general = false)
    {
        $query = Product::whereStatus(true);
        $query->select(['id', 'name', 'category_id', 'slug', 'img', 'art', 'minimal_price']);

        if ($category_id) {
            $categoryIds = Category::whereParentId($category_id)->get(['id']);
            if ($categoryIds->count())
                $categoryIds = $categoryIds->pluck('id');

            $categoryIds->push($category_id);

            $query->whereHas('category', function ($q) use ($categoryIds) {
                $q->whereIn('id', $categoryIds);
            });
        }

        if ($search)
            $query->where('name', 'like', '%' . $search . '%')
                ->orWhere('art', 'like', '%' . $search . '%');

        if (isset($filters['filter_slug'])) {
            $filterItem = FilterItems::whereSlug($filters['filter_slug'])->first('id');
            if ($filterItem) {
                ;
                $query->whereHas('filterItems', function ($q) use ($filterItem) {
                    $q->where('filter_items_id', $filterItem->id);
                });
            }
        }

        if (isset($filters['order']) && $filters['order'] === 'art')
            $query->orderBy('art');

        if (isset($filters['order']) && $filters['order'] === 'minimal_price')
            $query->orderBy('minimal_price');

        if (isset($filters['order']) && $filters['order'] === 'popular')
            $query->withCount('views')->orderByDesc('views_count');

        if ($limit)
            return self::mapProducts($query->limit($limit)->get());


        $products = $query->paginate(5);

        return self::getPaginator($products) + ['products' => self::mapProducts($products)];
    }

    public static function mapProducts($products) {
        return $products->map(function ($product) {

            $category_parent = $product->category->parent;
            $category_child = $product->category;

            return [
                'id' => $product->id,
                'href' => route('product', [$category_parent->slug, $category_child->slug, $product->slug]),
                'img_path' => $product->img_path,
                'name' => $product->name,
                'art' => $product->art,
                'min_price' => $product->minimal_price_string,
                'is_favorite' => $product->is_favorite
            ];
        });
    }

    public static function getAttributeTree($id)
    {
        $product = self::findOrFail($id);
        $attributes = $product->attributes()->with('columns')->get();

        if ($attributes->count()) {
            $attribute = $attributes->shift()->toArray();
            $attribute['rows'] = AttributeRow::whereAttributeId($attribute['id'])
                ->with('cells')
                ->orderBy('sort')
                ->get()->toArray();

            foreach ($attribute['rows'] as $index => $generalRow) { /// перебираем полученные строки
                $subAttributes = [];

                foreach ($attributes->toArray() as $subAttribute) {
                    $subAttribute['rows'] = AttributeRow::whereAttributeId($subAttribute['id'])
                        ->whereParentId($generalRow['id'])
                        ->with('cells')
                        ->orderBy('sort')
                        ->get()->toArray();
                    $subAttributes[] = $subAttribute;
                }
                $attribute['rows'][$index]['rows'] = $subAttributes;
            }
            return $attribute;
        }

        return [];
    }

    public static function getAttributesForProduct($id)
    {
        $product = self::findOrFail($id);
        return $product->attributes()->with('columns', 'rows.cells')->get();
    }

    public static function updateMinPrice($product_id)
    {
        $product = self::find($product_id);
        if ($product->generalAttribute)
            $product->minimal_price = $product->generalAttribute->minPriceRow->sum;
        $product->save();
        return true;
    }

    public static function getTotalProducts()
    {
        return self::numberFormat(self::count());
    }

}
