<?php

namespace App\Models;

use App\Traits\NumberFormatTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory, NumberFormatTrait;

    protected $fillable = [
        'session_key',
        'order_id',
        'product_id',
        'name',
        'count',
        'sum',
        'sum_sale',
        'detail'
    ];

    protected $casts = [
        'detail' => 'array',
        'sum' => 'integer',
        'sum_sale' => 'integer',
        'count' => 'integer',
    ];

    protected $appends = [
        'sum_string',
        'total',
        'total_sale',
        'total_string',
        'total_sale_string',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getSumStringAttribute()
    {
        return $this->sum ? self::maney($this->sum) : null;
    }

    public function getTotalAttribute()
    {
        if ($this->sum && $this->count) {
            return $this->sum * $this->count;
        }
        return 0;
    }

    public function getTotalStringAttribute()
    {
        if ($this->sum && $this->count) {
            return self::maney($this->sum * $this->count);
        }
        return null;
    }

    public function getTotalSaleAttribute()
    {
        if ($this->sum_sale && $this->count) {
            return $this->sum_sale * $this->count;
        }
        return 0;
    }

    public function getTotalSaleStringAttribute()
    {
        if ($this->sum_sale && $this->count) {
            return self::maney($this->sum_sale * $this->count);
        }
        return null;
    }
}
