<?php

namespace App\Models;

use App\Traits\NumberFormatTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory, NumberFormatTrait;

    protected $fillable = [
        'name',
        'phone',
        'information'
    ];

    public static function getTotalApplications()
    {
        return self::numberFormat(self::count());
    }
}
