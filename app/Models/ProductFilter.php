<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFilter extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'filter_items_id',
    ];

    public static function updateProductItems($product_id, $filters)
    {
        $currentFilters = self::whereProductId($product_id)->get()->keyBy('filter_items_id');

        foreach ($filters as $filterId) {
            if (!$currentFilters->firstWhere('filter_items_id', $filterId)) {
                self::create([
                    'product_id' => $product_id,
                    'filter_items_id' => $filterId,
                ]);
            }
            $currentFilters->forget($filterId);
        }

        foreach ($currentFilters as $currentFilter) {
            $currentFilter->delete();
        }

        return true;
    }
}
