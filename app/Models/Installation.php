<?php

namespace App\Models;

use App\Traits\NumberFormatTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Installation extends Model
{
    use HasFactory, NumberFormatTrait;

    protected $fillable = [
        'product_id',
        'sort',
        'name',
        'price',
    ];

    protected $appends = [
        'price_string',
    ];

    public function getPriceStringAttribute()
    {
        return self::maney($this->price);
    }

}
