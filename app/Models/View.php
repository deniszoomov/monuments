<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class View extends Model
{
    use HasFactory;

    protected $fillable = [
        'session_key',
        'product_id',
        'updated_at'
    ];

    protected $casts = [
        'updated_at' => 'datetime',
    ];

    /**
     * Просмотры продуктов
     * за последние 30 дней
     *
     * @return array|array[]
     */
    public static function getProductViewLast30Days()
    {
        $startDate = Carbon::now()->subDays(30);
        $products = self::whereDate('updated_at', '>', $startDate)
            ->select([
                'product_id',
                DB::raw('DATE_FORMAT(updated_at, "%d-%m") as date')
            ])->orderBy('updated_at')->get();

        $views = collect($products)->groupBy('date')
            ->map(function ($items) {
                return $items->count();
            });

        $result = [ 'data' => [], 'categories' => [] ];
        foreach ($views as $category => $view) {
            $result['data'][] = $view;
            $result['categories'][] = $category;
        }

        return $result;
    }

}
