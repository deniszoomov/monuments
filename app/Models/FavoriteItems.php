<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavoriteItems extends Model
{
    use HasFactory;

    protected $fillable = [
        'session_key',
        'product_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
