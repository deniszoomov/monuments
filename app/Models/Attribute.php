<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'sort',
        'product_id',
    ];

    public function columns()
    {
        return $this->hasMany(AttributeColumn::class)->orderBy('sort');
    }

    public function rows()
    {
        return $this->hasMany(AttributeRow::class)->orderBy('sort');
    }

    public function minPriceRow()
    {
        return $this->hasOne(AttributeRow::class)->orderBy('sum')->withDefault([
            'sum' => 0
        ]);
    }
}
