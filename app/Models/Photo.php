<?php

namespace App\Models;

use App\Traits\PaginatorTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory, PaginatorTrait;

    protected $fillable = [
        'category_id',
        'preview_img',
        'origin_img',
    ];

    protected $appends = [
        'preview_img_path',
        'origin_img_path',
    ];

    public function filterItems()
    {
        return $this->belongsToMany(FilterItems::class, PhotoFilter::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getPreviewImgPathAttribute()
    {
        return $this->preview_img ? url($this->preview_img) : null;
    }

    public function getOriginImgPathAttribute()
    {
        return $this->origin_img ? url($this->origin_img) : null;
    }

    public static function getForCatalog($category_id = null, $filters = [], $limit = 12)
    {
        $query = Photo::with([
            'category' => function ($q) {
                $q->select(['id', 'title']);
            },
            'filterItems' => function ($q) {
                $q->select(['name']);
            }])->latest()
            ->limit($limit);
        if ($category_id)
            $query->whereCategoryId($category_id);

        if (isset($filters['filter_slug'])) {
            $filterItem = FilterItems::whereSlug($filters['filter_slug'])->first('id');
            if ($filterItem) {
                ;
                $query->whereHas('filterItems', function ($q) use ($filterItem) {
                    $q->where('filter_items_id', $filterItem->id);
                });
            }
        }
        return $query->get();
    }

    public static function getForPage($filter_slug = null)
    {
        $query = Photo::query();
        if ($filter_slug) {
            $filterItem = FilterItems::whereSlug($filter_slug)->first('id');
            $query->whereHas('filterItems', function ($q) use ($filterItem) {
                $q->where('filter_items_id', $filterItem->id);
            });
        }
        $photos = $query->paginate(20);

        return self::getPaginator($photos) + ['photos' => $photos->map(function ($photo) {
                return [
                    'id' => $photo->id,
                    'preview_img_path' => $photo->preview_img_path,
                    'origin_img_path' => $photo->origin_img_path,
                ];
            })
        ];
    }
}
