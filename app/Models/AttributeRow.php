<?php

namespace App\Models;

use App\Traits\NumberFormatTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeRow extends Model
{
    use HasFactory, NumberFormatTrait;

    protected $fillable = [
        'sum',
        'up_installation',
        'sort',
        'about',
        'color', // gray, white
        'attribute_id',
        'parent_id',
    ];

    protected $appends = [
        'price_string',
    ];

    public function setUpInstallationAttribute($value)
    {
        $this->attributes['up_installation'] = $value ?? 0;
    }

    public function getPriceStringAttribute()
    {
        return self::maney($this->sum);
    }

    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function cells()
    {
        return $this->hasMany(AttributeCell::class);
    }
}
