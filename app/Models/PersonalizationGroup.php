<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalizationGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'personalization_id',
        'name',
    ];

    public function items()
    {
        return $this->hasMany(PersonalizationItem::class, 'personalization_group_id', 'id')
            ->orderBy('personalization_items.sum');
    }
}
