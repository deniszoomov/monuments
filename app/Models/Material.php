<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'img',
        'percent',
        'sort',
    ];

    protected $appends = [
        'img_path',
    ];

    public function getImgPathAttribute()
    {
        return $this->img ? url($this->img) : null;
    }

}
