<?php

namespace App\Models;

use App\Traits\NumberFormatTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory, NumberFormatTrait;

    protected $fillable = [
        'session_key',
        'name',
        'phone',
        'email',
        'where_apply',
        'payment_method',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    protected $appends = [
        'full_title',
        'created_string',
    ];

    public function getFullTitleAttribute()
    {
        return 'Заказ №'.$this->id;
    }

    public function getCreatedStringAttribute()
    {
        return $this->created_at ? $this->created_at->format('d.m.Y H:i') : null;
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public static function getTotalClients()
    {
        return self::numberFormat(self::distinct()->select('session_key')->count());
    }

    public static function getTotalOrders()
    {
        return self::numberFormat(self::count());
    }
}
