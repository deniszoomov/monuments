<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personalization extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'img',
        'description',
        'sort',
    ];

    protected $appends = [
        'img_path',
    ];

    public function getImgPathAttribute()
    {
        return $this->img ? url($this->img) : null;
    }

    public function groups()
    {
        return $this->hasMany(PersonalizationGroup::class);
    }
}
