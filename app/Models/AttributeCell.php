<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeCell extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'attribute_column_id',
        'attribute_row_id',
    ];
}
