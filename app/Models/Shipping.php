<?php

namespace App\Models;

use App\Traits\NumberFormatTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use HasFactory, NumberFormatTrait;

    protected $fillable = [
        'sort',
        'name',
        'price',
    ];

    protected $appends = [
        'price_string',
    ];

    public function getPriceStringAttribute()
    {
        return self::maney($this->price);
    }
}
