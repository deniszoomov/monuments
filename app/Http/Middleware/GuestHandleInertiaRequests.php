<?php

namespace App\Http\Middleware;

use App\Repositories\BasketRepository;
use App\Repositories\FiltersRepository;
use App\Repositories\RouteRepository;
use App\Repositories\SettingsRepository;
use App\Services\RouteServices;
use Illuminate\Http\Request;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;

class GuestHandleInertiaRequests extends Middleware
{

    private BasketRepository $basketRepository;
    private RouteRepository $routeRepository;
    private FiltersRepository $filtersRepository;
    private SettingsRepository $settingsRepository;

    public function __construct()
    {
        $this->routeRepository = new RouteRepository;
        $this->basketRepository = new BasketRepository;
        $this->filtersRepository = new FiltersRepository;
        $this->settingsRepository = new SettingsRepository;
    }

    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     *
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            'ziggy' => (new Ziggy)->toArray(),
            'routes' => function () {
                return $this->routeRepository->getRoutes();
            },
            'basket' => function () {
                return $this->basketRepository->getBasket([
                    'id', 'name', 'count', 'sum', 'sum_sale'
                ]);
            },
            'filters' => function () {
                return $this->filtersRepository->getFilters();
            },
            'settings' => function () {
                return $this->settingsRepository->get();
            }
        ]);
    }
}
