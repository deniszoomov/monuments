<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AttributeColumn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AttributeColumnsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'sort' => 'required',
                'attribute_id' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $filter = new AttributeColumn();
            $filter->fill($request->all());
            $filter->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'sort' => 'required',
                'attribute_id' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $filter = AttributeColumn::find($id);
            $filter->fill($request->all());
            $filter->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            AttributeColumn::find($id)->delete();
            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
