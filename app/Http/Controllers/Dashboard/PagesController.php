<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Repositories\RouteRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Dashboard/Settings/Pages/Index', [
            'meta' => [
                'title' => 'Страницы',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Страницы'],
                ]
            ],
            'pages' => (new RouteRepository)->getStaticSlugPages(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Inertia\Response
     */
    public function edit($slug)
    {
        $page = Page::whereSlug($slug)->first();
        $pageDefault = (new RouteRepository)->whereSlug($slug);
        $title = $page ? $page->name : $pageDefault['name'];


        return Inertia::render('Dashboard/Settings/Pages/ShowOrCreate/Index', [
            'meta' => [
                'title' => $title,
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Страницы', 'href' => route('dashboard.settings.pages.index')],
                    ['name' => $title],
                ]
            ],
            'slug' => $pageDefault['slug'],
            'page' => $page
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $slug)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $page = Page::whereSlug($slug)->first();
            if (!$page)
                $page = new Page();

            $page->fill($request->all());
            $page->save();

            return redirect()->route('dashboard.settings.pages.edit', $slug)->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
