<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Order;
use App\Models\Product;
use App\Models\View;
use Illuminate\Http\Request;
use Inertia\Inertia;

class IndexController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard/Main/Index',[
            'meta' => [
                'title' => 'Главная'
            ],
            'orders' => Order::withSum('items', 'sum')
                ->withSum('items', 'sum_sale')
                ->orderbyDesc('created_at')
                ->paginate(5),
            'applications' => Application::latest()->paginate(2),
            'product_view' => View::getProductViewLast30Days(),
            'total_information' => [
                'clients' => Order::getTotalClients(),
                'orders' => Order::getTotalOrders(),
                'products' => Product::getTotalProducts(),
                'applications' => Application::getTotalApplications()
            ]
        ]);
    }
}
