<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Dashboard/Catalog/Categories/Index',[
            'meta' => [
                'title' => 'Категории',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Категории'],
                ]
            ],
            'categories' => Category::getNameListWithChildren(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Dashboard/Catalog/Categories/ShowOrCreate/Index',[
            'meta' => [
                'title' => 'Новая категория',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Категории', 'href' => route('dashboard.categories.index')],
                    ['name' => 'Новая категория'],
                ]
            ],
            'categories' => Category::getNameListWithChildren(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->title)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required|unique:categories,slug',
                'title' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $category = new Category();
            $category->fill($request->all());
            $category->save();

            return redirect()->route('dashboard.categories.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return Inertia::render('Dashboard/Catalog/Categories/ShowOrCreate/Index',[
            'meta' => [
                'title' => $category->name,
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Категории', 'href' => route('dashboard.categories.index')],
                    ['name' => $category->name],
                ]
            ],
            'category' => $category,
            'categories' => Category::getNameListWithChildren($category->id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->title)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required|unique:categories,slug,'.$id,
                'title' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $category = Category::find($id);
            $category->fill($request->all());
            $category->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            Category::find($id)->delete();
            return redirect()->route('dashboard.categories.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

}
