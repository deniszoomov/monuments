<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ApplicationsController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard/Sales/Applications/Index',[
            'meta' => [
                'title' => 'Заявки',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Заявки'],
                ]
            ],
            'applications' => Application::latest()->get(), //#todo pagination
        ]);
    }
}
