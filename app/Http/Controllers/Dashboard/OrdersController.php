<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Dashboard/Sales/Orders/Index',[
            'meta' => [
                'title' => 'Заказы',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Заказы'],
                ]
            ],
            'orders' => Order::withSum('items', 'sum')
                ->withSum('items', 'sum_sale')
                ->orderbyDesc('created_at')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        return Inertia::render('Dashboard/Sales/Orders/Show/Index',[
            'meta' => [
                'title' => $order->full_title,
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Категории', 'href' => route('dashboard.categories.index')],
                    ['name' => $order->full_title],
                ]
            ],
            'order' => $order,
            'items' => $order->items()->with('product')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            Order::find($id)->delete();
            return redirect()->route('dashboard.orders.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
