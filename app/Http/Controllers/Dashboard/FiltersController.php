<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Filter;
use App\Models\FilterItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;

class FiltersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Dashboard/Catalog/Filters/Index',[
            'meta' => [
                'title' => 'Фильтры',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Фильтры'],
                ]
            ],
            'filters' => Filter::with('category')->orderBy('sort')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Dashboard/Catalog/Filters/ShowOrCreate/Index',[
            'meta' => [
                'title' => 'Новый фильтр',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Фильтры', 'href' => route('dashboard.filters.index')],
                    ['name' => 'Новый фильтр'],
                ]
            ],
            'categories' => Category::whereNull('parent_id')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->name)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'category_id' => 'required',
                'slug' => 'required|unique:filters,slug',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $filter = new Filter();
            $filter->fill($request->all());
            $filter->save();

            return redirect()->route('dashboard.filters.edit', $filter->id)->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $filter = Filter::whereId($id)->with('items')->first();
        return Inertia::render('Dashboard/Catalog/Filters/ShowOrCreate/Index',[
            'meta' => [
                'title' => $filter->name,
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Фильтры', 'href' => route('dashboard.filters.index')],
                    ['name' => $filter->name],
                ]
            ],
            'filter' => $filter,
            'categories' => Category::whereNull('parent_id')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->name)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required|unique:filters,slug,'.$id,
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $category = Filter::find($id);
            $category->fill($request->all());
            $category->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            Filter::find($id)->delete();
            return redirect()->route('dashboard.filters.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function storeItem(Request $request)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->name)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'filter_id' => 'required',
                'slug' => 'required|unique:filters,slug',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $filter = new FilterItems();
            $filter->fill($request->all());
            $filter->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function updateItem(Request $request, $item_id)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->name)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'filter_id' => 'required',
                'slug' => 'required|unique:filter_items,slug,'.$item_id,
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $category = FilterItems::find($item_id);
            $category->fill($request->all());
            $category->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function destroyItem($item_id)
    {
        try {
            FilterItems::find($item_id)->delete();
            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
