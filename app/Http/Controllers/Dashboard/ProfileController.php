<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class ProfileController extends Controller
{

    public function index()
    {
        return Inertia::render('Dashboard/Profile/Index',[
            'meta' => [
                'title' => 'Профиль',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Профиль'],
                ]
            ]
        ]);
    }

    public function update(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
        ]);

        $user->fill($request->all());
        $user->save();

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'current_password' => 'required_with:password|password|max:8',
            'password' => 'confirmed|max:8',

        ]);

        if (Hash::check($request->current_password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            return back()->with('success', 'Успешно');
        }
        return back()->with('error', 'Проверьте правильность текущего пароля');
    }
}
