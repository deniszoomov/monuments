<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Filter;
use App\Models\Photo;
use App\Models\PhotoFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Dashboard/Catalog/Photos/Index', [
            'meta' => [
                'title' => 'Примеры работ',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Фото'],
                ]
            ],
            'photos' => Photo::with([
                'category' => function ($q) {
                    $q->select(['id', 'title']);
                },
                'filterItems' => function ($q) {
                    $q->select(['name']);
                }])
                ->latest()
                ->paginate(20),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Dashboard/Catalog/Photos/ShowOrCreate/Index', [
            'meta' => [
                'title' => 'Новое фото',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Фото', 'href' => route('dashboard.photos.index')],
                    ['name' => 'Новое фото'],
                ]
            ],
            'categories' => Category::getNameListWithChildren(),
            'filters' => Filter::with('items')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'preview_img' => 'required',
                'origin_img' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $photo = new Photo();
            $photo->fill($request->all());
            $photo->save();

            PhotoFilter::updatePhotoItems($photo->id, $request->filters);

            return redirect()->route('dashboard.photos.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $photo = Photo::whereId($id)
            ->with('filterItems')
            ->first();
        return Inertia::render('Dashboard/Catalog/Photos/ShowOrCreate/Index', [
            'meta' => [
                'title' => 'Фото ' . $photo->id,
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Фото', 'href' => route('dashboard.photos.index')],
                    ['name' => 'Фото' . $photo->id],
                ]
            ],
            'photo' => $photo,
            'categories' => Category::getNameListWithChildren(),
            'filters' => Filter::with('items')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'preview_img' => 'required',
                'origin_img' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $photo = Photo::find($id);
            $photo->fill($request->all());
            $photo->save();

            PhotoFilter::updatePhotoItems($photo->id, $request->filters);

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            Photo::find($id)->delete();
            return redirect()->route('dashboard.photos.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
