<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Reviews;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ReviewsController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard/Catalog/Reviews/Index',[
            'meta' => [
                'title' => 'Отзывы',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Отзывы'],
                ]
            ],
            'reviews' => Reviews::latest()->get(), //#todo pagination
        ]);
    }
}
