<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AttributeCell;
use App\Models\AttributeRow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AttributeValueController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'attribute_id' => 'required',
                'sort' => 'required',
                'color' => 'required',
                'cells.*.attribute_column_id' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $row = new AttributeRow();
            $row->fill($request->all());
            $row->save();

            foreach ($request->cells as $data) {
                $cell = new AttributeCell();
                $data['attribute_row_id'] = $row->id;
                $cell->fill($data);
                $cell->save();
            }

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'attribute_id' => 'required',
                'sort' => 'required',
                'color' => 'required',
                'cells.*.attribute_column_id' => 'required',
                'cells.*.attribute_row_id' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $row = AttributeRow::find($id);
            $row->fill($request->all());
            $row->save();

            foreach ($request->cells as $data) {
                $cell = AttributeCell::find($data['id']);
                $data['attribute_row_id'] = $row->id;
                $cell->fill($data);
                $cell->save();
            }

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            AttributeRow::find($id)->delete();
            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
