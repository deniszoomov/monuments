<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\PersonalizationItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PersonalizationItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'personalization_group_id' => 'required',
                'name' => 'required',
                'sum' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $personalization = new PersonalizationItem();
            $personalization->fill($request->all());
            $personalization->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'personalization_group_id' => 'required',
                'name' => 'required',
                'sum' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $personalization = PersonalizationItem::find($id);
            $personalization->fill($request->all());
            $personalization->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            PersonalizationItem::find($id)->delete();
            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
