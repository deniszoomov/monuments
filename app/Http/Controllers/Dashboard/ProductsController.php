<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Filter;
use App\Models\Installation;
use App\Models\Product;
use App\Models\ProductFilter;
use App\Traits\FileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;

class ProductsController extends Controller
{
    use FileTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $query = Product::query();
        if ($request->name) $query->where('name', 'like', '%'.$request->name.'%');
        if ($request->category_id) $query->where('category_id', $request->category_id);

        return Inertia::render('Dashboard/Catalog/Products/Index',[
            'meta' => [
                'title' => 'Продукты',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Продукты'],
                ]
            ],
            'products' => $query->get(),
            'categories' => Category::getNameListOnlyChildren(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {

        return Inertia::render('Dashboard/Catalog/Products/ShowOrCreate/Index',[
            'meta' => [
                'title' => 'Новый продукт',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Продукты', 'href' => route('dashboard.products.index')],
                    ['name' => 'Новый продукт'],
                ]
            ],
            'categories' => Category::getNameListOnlyChildren(),
            'filters' => Filter::with('items')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->name).'-'.Str::slug($request->art)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required|unique:categories,slug',
                'category_id' => 'required',
                'status' => 'required',
                'art' => 'required',
                'img' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $product = new Product();
            $product->fill($request->all());
            $product->save();

            ProductFilter::updateProductItems($product->id, $request->filters);
            Product::updateMinPrice($product->id);

            return redirect()->route('dashboard.products.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $product = Product::whereId($id)
            ->with('attributes.columns', 'attributes.rows.cells', 'installations', 'filterItems')
            ->first();
        $attributeTree = Product::getAttributeTree($id);
        $Installations = $product->installations()->get();

        return Inertia::render('Dashboard/Catalog/Products/ShowOrCreate/Index',[
            'meta' => [
                'title' => $product->name,
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Продукты', 'href' => route('dashboard.products.index')],
                    ['name' => $product->name],
                ]
            ],
            'product' => $product,
            'attributeTree' => $attributeTree,
            'installations' => $Installations,
            'filters' => Filter::with('items')->get(),
            'categories' => Category::getNameListOnlyChildren(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $request->request->add(['slug' => Str::slug($request->name).'-'.Str::slug($request->art)]);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required|unique:categories,slug,'.$id,
                'category_id' => 'required',
                'status' => 'required',
                'art' => 'required',
                'img' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $product = Product::find($id);
            $product->fill($request->all());
            $product->save();

            ProductFilter::updateProductItems($product->id, $request->filters);
            Product::updateMinPrice($product->id);

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $product = Product::find($id);
            $product->delete();
            $this->deleteFile($product->img);
            return redirect()->route('dashboard.products.index')->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
