<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class RegistersController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard/Catalog/Registers/Index',[
            'meta' => [
                'title' => 'Оформление заказа',
                'crumb' => [
                    ['name' => 'Главная', 'href' => route('dashboard.index')],
                    ['name' => 'Оформление заказа'],
                ]
            ],
            'registers' => Register::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $register = new Register();
            $register->fill($request->all());
            $register->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                $error = implode('<br>', $validator->getMessageBag()->all());
                throw new \Exception($error);
            }

            $register = Register::find($id);
            $register->fill($request->all());
            $register->save();

            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            Register::find($id)->delete();
            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
