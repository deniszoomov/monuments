<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Reviews;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ContactsController extends Controller
{
    protected const slug = 'contacts';

    public function index()
    {
        $page = Page::whereSlug(self::slug)->firstOrFail();

        return Inertia::render('Guest/Contacts',[
            'meta' => ['title' => $page->name],
            'title' => $page->name,
            'description' => $page->description,
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => $page->name],
            ]
        ]);
    }
}
