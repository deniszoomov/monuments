<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PolicyController extends Controller
{
    protected const slug = 'policy';

    public function index()
    {
        $page = Page::whereSlug(self::slug)->firstOrFail();

        return Inertia::render('Guest/Policy',[
            'meta' => ['title' => $page->name],
            'title' => $page->name,
            'description' => $page->description,
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => $page->name],
            ]
        ]);
    }
}
