<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Repositories\BasketRepository;
use Illuminate\Http\Request;
use Inertia\Inertia;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
        ]);

        try {
            (new BasketRepository)->createOrder($request->all());
            return redirect()->route('order.successful');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function successful()
    {
        return Inertia::render('Guest/Basket/Result',[
            'meta' => ['title' => 'Спасибо за заказ'],
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => 'Заказ'],
            ]
        ]);
    }

}
