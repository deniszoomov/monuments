<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Repositories\FavoriteRepository;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FavoriteController extends Controller
{
    public function index()
    {
        return Inertia::render('Guest/Favorites',[
            'meta' => ['title' => 'Ваши избранные товары'],
            'title' => 'Ваши избранные товары',
            'products' => (new FavoriteRepository)->get(),
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => 'Избранные товары'],
            ]
        ]);
    }

    public function store($product_id)
    {
        (new FavoriteRepository)->set($product_id);
        return back();
    }

    public function destroy($product_id)
    {
        (new FavoriteRepository)->destroy($product_id);
        return back();
    }

    public function destroyAll()
    {
        (new FavoriteRepository)->destroyAll();
        return back();
    }
}
