<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Reviews;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ReviewsController extends Controller
{

    protected const slug = 'reviews';

    public function index()
    {
        $page = Page::whereSlug(self::slug)->firstOrFail();

        return Inertia::render('Guest/Reviews',[
            'meta' => ['title' => $page->name],
            'title' => $page->name,
            'description' => $page->description,
            'reviews' => Reviews::all(),
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => $page->name],
            ]
        ]);
    }

    public function store(Request $request)
    {
        try {
            $model = new Reviews();
            $model->fill($request->all());
            $model->save();
            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
