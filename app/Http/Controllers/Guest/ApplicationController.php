<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function store(Request $request)
    {
        try {
            Application::create($request->all());
            return back()->with('success', 'Успешно');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
