<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Repositories\ViewsRepository;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ViewController extends Controller
{
    public function index()
    {
        return Inertia::render('Guest/Viewed',[
            'meta' => ['title' => 'Ваша корзина'],
            'data' => (new ViewsRepository)->get(),
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => 'Корзина'],
            ]
        ]);
    }
}
