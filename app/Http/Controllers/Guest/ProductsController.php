<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Material;
use App\Models\Personalization;
use App\Models\Product;
use App\Models\Shipping;
use App\Repositories\ViewsRepository;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductsController extends Controller
{
    public function show($category_slug, $children_slug, $product_slug)
    {
        $category = Category::whereSlug($category_slug)->firstOrFail();
        $children = Category::whereSlug($children_slug)->firstOrFail();
        $product = Product::whereSlug($product_slug)->firstOrFail();
        $attribute = Product::getAttributeTree($product->id);
        $attributes = Product::getAttributesForProduct($product->id);
        $discounts = Discount::all();
        $products = Product::getForCatalog($category->id, null, [], 6);

        (new ViewsRepository)->set($product->id);

        return Inertia::render('Guest/ProductDetail', [
            'meta' => [
                'title' => $product->meta_title,
                'description' => $product->meta_description,
                'keywords' => $product->meta_keywords,
            ],
            'product' => $product,
            'attribute' => $attribute,
            'attributes' => $attributes,
            'materials' => $product->has_material ? Material::orderBy('sort')->get() : null,
            'shippings' => $product->has_shipping ? Shipping::orderBy('sort')->get() : null,
            'installations' => $product->has_installation ? $product->installations()->get() : null,
            'personalizations' => $product->has_personalizations ? Personalization::orderBy('sort')->with('groups.items')->get() : null,
            'discounts' => $discounts,
            'products' => $products,
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => $category->title, 'href' => route('catalog.category', $category->slug)],
                ['name' => $children->title, 'href' => route('catalog.category-children', [$category->slug, $children->slug])],
                ['name' => $product->name . ' ('. $product->art .')'],
            ]
        ]);
    }
}
