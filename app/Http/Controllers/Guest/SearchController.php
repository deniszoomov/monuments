<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Guest/Search', [
            'meta' => ['title' => 'Поиск товаров'],
            'data' => Product::getForCatalog(null, $request->search),
            'search' => $request->search,
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => 'Поиск'],
            ]
        ]);
    }
}
