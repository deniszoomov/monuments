<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;
use App\Models\Photo;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;

class CatalogController extends Controller
{
    public function welcome()
    {
        $page = Page::whereSlug('general_page')->first();
        return Inertia::render('Guest/Welcome', [
            'meta' => ['title' => 'Изготовление памятников, фото и цены в Москва'],
            'title' => $page ? $page->name : 'Каталог',
            'about' => $page ? $page->description : '',
            'data' => Product::getForCatalog(),
            'categories' => Category::getCategoriesForList(),
            'photos' => Photo::getForCatalog(),
        ]);
    }

    public function category($category_slug, Request $request)
    {
        $category = Category::whereSlug($category_slug)->firstOrFail();

        return Inertia::render('Guest/Welcome', [
            'meta' => [
                'title' => $category->meta_title,
                'description' => $category->meta_description,
                'keywords' => $category->meta_keywords,
            ],
            'title' => $category->title,
            'about' => $category->about,
            'data' => Product::getForCatalog($category->id, null, $request->all()),
            'categories' => Category::getCategoriesForList(),
            'photos' => Photo::getForCatalog($category->id, $request->all()),
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => $category->title],
            ]
        ]);
    }

    public function categoryChildren($category_slug, $children_slug, Request $request)
    {
        $category = Category::whereSlug($category_slug)->firstOrFail();
        $children = Category::whereSlug($children_slug)->firstOrFail();

        return Inertia::render('Guest/Welcome', [
            'meta' => [
                'title' => $children->meta_title,
                'description' => $children->meta_description,
                'keywords' => $children->meta_keywords,
            ],
            'title' => $children->title,
            'about' => $children->about,
            'data' => Product::getForCatalog($children->id, null, $request->all()),
            'categories' => Category::getCategoriesForList(),
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => $category->title, 'href' => route('catalog.category', $category->slug)],
                ['name' => $children->title],
            ]
        ]);
    }
}
