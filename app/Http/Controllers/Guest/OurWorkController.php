<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Filter;
use App\Models\Page;
use App\Models\Photo;
use Illuminate\Http\Request;
use Inertia\Inertia;

class OurWorkController extends Controller
{
    protected const slug = 'ourwork';

    public function index(Request $request)
    {
        $page = Page::whereSlug(self::slug)->firstOrFail();

        return Inertia::render('Guest/OurWork',[
            'meta' => ['title' => $page->name],
            'filters' => Filter::with('items')->orderBy('sort')->get(),
            'title' => $page->name,
            'description' => $page->description,
            'data' => Photo::getForPage($request->filter_slug),
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => $page->name],
            ]
        ]);
    }
}
