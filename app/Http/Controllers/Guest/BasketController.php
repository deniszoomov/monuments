<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Payment;
use App\Models\Register;
use App\Repositories\BasketRepository;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BasketController extends Controller
{
    public function index()
    {
        return Inertia::render('Guest/Basket/Index',[
            'meta' => ['title' => 'Ваша корзина'],
            'basket' => (new BasketRepository)->getBasket(),
            'discounts' => Discount::all(),
            'registers' => Register::all(),
            'payments' => Payment::all(),
            'crumbs' => [
                ['name' => 'Главная', 'href' => route('catalog')],
                ['name' => 'Корзина'],
            ]
        ]);
    }

    public function store(Request $request, $product_id)
    {
        (new BasketRepository)->setItem(
            $product_id,
            $request->count,
            $request->sum,
            $request->sum_sale,
            $request->detail,
        );
        return back();
    }

    public function destroy($item_id)
    {
        (new BasketRepository)->destroyItem($item_id);
        return back();
    }

    public function addCount($item_id)
    {
        (new BasketRepository)->addCount($item_id);
        return back();
    }

    public function remCount($item_id)
    {
        (new BasketRepository)->remCount($item_id);
        return back();
    }
}
