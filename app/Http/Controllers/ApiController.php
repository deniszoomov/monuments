<?php

namespace App\Http\Controllers;

use App\Traits\FileTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ApiController extends Controller
{
    use FileTrait;

    public function saveImage(Request $request)
    {
        $request->validate([
            'folder' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif,svg,webp|required|max:100000'
        ]);

        $uploadImage = $request->file('image');

        $image = Image::make($uploadImage->path());

        if ((bool) $request->keep_margins && $request->height && $request->width) {
            $image->resize((int) $request->width, (int) $request->height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $image = Image::canvas((int) $request->width, (int) $request->height)->insert($image, 'center');
        } else if ($request->height && $request->width) {
            $image->fit((int) $request->width, (int) $request->height);
        }

        $image->encode('webp', 90);

        $name = Carbon::now()->timestamp .'.webp';
        $imagePath = "images/$request->folder/$name";

        $this->putFile($imagePath, $image);

        return response()->json([
            'path' => $imagePath,
            'full_path' => url($imagePath),
            'started_path' => '/'.$imagePath,
            'name' => $name]);
    }

    public function destroyFile(Request $request)
    {
        $request->validate([
            'path' => 'required'
        ]);
        $this->deleteFile($request->path);
        return response()->json(['success' => true]);
    }
}
