<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\View;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ViewsRepository
{
    private string $key;
    private View $view;
    private Product $product;
    protected const paginateLimit = 20;

    public function __construct()
    {
        $this->key = request()->cookie('session_key');
        $this->view = new View();
        $this->product = new Product();
    }

    public function set($product_id)
    {
        try {
            $view = $this->view->where('product_id', $product_id)
                ->where('session_key', $this->key)
                ->first();
            if ($view) {
                $view->updated_at = Carbon::now();
                $view->save();
            } else {
                $this->view::create([
                    'session_key' => $this->key,
                    'product_id' => $product_id,
                ]);
            }
            return true;
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    public function get()
    {
        $views = $this->view
            ->where('session_key', $this->key)
            ->latest()
            ->paginate(self::paginateLimit);

        $ids = $views->pluck('product_id');
        $products = $this->product->whereIn('id', $ids)->get();

        return $this->product::getPaginator($views)
            + ['products' => $this->product::mapProducts($products)];
    }
}
