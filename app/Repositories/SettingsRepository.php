<?php

namespace App\Repositories;

use hackerESQ\Settings\Facades\Settings;

class SettingsRepository
{
    public const SETTINGS = [
        'telegram' => '',
        'viber' => '',
        'phone_code' => '',
        'phone_number' => '',
        'schedule' => '',
        'address' => '',
        'email' => '',
    ];
    private Settings $setting;

    public function __construct()
    {
        $this->setting = new Settings;
    }

    public function get($key = null)
    {
        if ($key) {
            $result = $this->setting::get($key);
            return $result ? $result : self::SETTINGS[$key];
        }

        $result = $this->setting::get();
        return $result ? array_merge(self::SETTINGS, $result) : self::SETTINGS;
    }

    public function set($data)
    {
        $this->setting::set($data);
        return true;
    }

}
