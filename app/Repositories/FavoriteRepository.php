<?php

namespace App\Repositories;

use App\Models\FavoriteItems;
use App\Models\Product;
use App\Traits\NumberFormatTrait;

class FavoriteRepository
{

    use NumberFormatTrait;

    private $key;
    private FavoriteItems $model;

    public function __construct()
    {
        $this->key = request()->cookie('session_key');
        $this->model = new FavoriteItems;
    }

    public function get()
    {
        return $this->model
            ->whereSessionKey($this->key)
            ->with('product.category.parent')
            ->get()->map(function ($favorite) {
                $category_parent = $favorite->product->category->parent;
                $category_child = $favorite->product->category;

                $minPrice = 0;
                if ($favorite->product->generalAttribute)
                    $minPrice = $favorite->product->generalAttribute->minPriceRow->sum;

                return [
                    'id' => $favorite->product->id,
                    'href' => route('product', [$category_parent->slug, $category_child->slug, $favorite->product->slug]),
                    'img_path' => $favorite->product->img_path,
                    'name' => $favorite->product->name,
                    'art' => $favorite->product->art,
                    'is_favorite' => $favorite->product->is_favorite,
                    'min_price' => self::maney($minPrice),
                ];
            });
    }

    public function set($product_id)
    {
        if (!$this->findByProductId($product_id)) {
            $this->model::create([
                'session_key' => $this->key,
                'product_id' => $product_id,
            ]);
        }

        return true;
    }

    public function destroy($product_id)
    {
        if ($product = $this->findByProductId($product_id)) {
            $product->delete();
        }
        return true;
    }

    public function destroyAll()
    {
        $this->model->whereSessionKey($this->key)->delete();
        return true;
    }

    public function isSelected($product_id)
    {
        return !!$this->findByProductId($product_id);
    }

    private function findByProductId($product_id)
    {
        return $this->model
            ->whereSessionKey($this->key)
            ->whereProductId($product_id)
            ->first();
    }
}
