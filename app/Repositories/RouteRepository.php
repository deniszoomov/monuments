<?php

namespace App\Repositories;

use App\Models\Category;
use function route;

class RouteRepository
{

    /**
     * @return \array[][]
     */
    public function getRoutes(): array
    {
        $routes = [];
        $staticPages = collect($this->getStaticSlugPages())
            ->filter(function ($page) {
                return isset($page['is_left']);
            });

        foreach ($staticPages as $page) {
            $routes['general'][] = [
                'name' => $page['name'],
                'href' => route($page['slug']),
                'route' => $page['slug']
            ];
        }

        $routes['catalog'] = $this->getCatalog();

        $routes['footer'] = [
            'left' => $staticPages->where('is_left', true)->map(function ($page) {
                return [
                    'name' => $page['name'],
                    'href' => route($page['slug']),
                    'route' => $page['slug']
                ];
            }),
            'right' => $staticPages->where('is_left', false)->map(function ($page) {
                return [
                    'name' => $page['name'],
                    'href' => route($page['slug']),
                    'route' => $page['slug']
                ];
            }),
        ];

        return $routes;
    }

    public function getCatalog()
    {
        return Category::whereNull('parent_id')
            ->select(['id', 'name', 'slug', 'sort'])
            ->orderBy('sort')
            ->with(['childrens' => function ($query) {
                $query->select(['id', 'name', 'slug', 'sort', 'parent_id']);
            }])
            ->get()->map(function ($category) {
                return [
                    'title' => $category->name,
                    'href' => route('catalog.category', $category->slug),
                    'items' => $category->childrens->map(function ($children) use ($category) {
                        return [
                            'title' => $children->name,
                            'href' => route('catalog.category-children', [$category->slug, $children->slug]),
                        ];
                    })
                ];
            });
    }

    public function getStaticSlugPages()
    {
        return [
            ['name' => 'О нас', 'slug' => 'about-us', 'is_left' => false],
            ['name' => 'Сроки', 'slug' => 'terms', 'is_left' => true],
            ['name' => 'Установка', 'slug' => 'installation', 'is_left' => true],
            ['name' => 'Доставка', 'slug' => 'delivery', 'is_left' => true],
            ['name' => 'Гарантия', 'slug' => 'guarantee', 'is_left' => false],
            ['name' => 'Оплата', 'slug' => 'payment', 'is_left' => true],
            ['name' => 'Скидки', 'slug' => 'discounts', 'is_left' => true],
            ['name' => 'Наши работы', 'slug' => 'ourwork', 'is_left' => false],
            ['name' => 'Отзывы', 'slug' => 'reviews', 'is_left' => false],
            ['name' => 'Контакты', 'slug' => 'contacts', 'is_left' => false],
            ['name' => 'Текст на гравную страницу', 'slug' => 'general_page'],
            ['name' => 'Правовая информация', 'slug' => 'policy'],
        ];
    }

    public function whereSlug($slug)
    {
        return collect($this->getStaticSlugPages())
            ->where('slug', $slug)->first();
    }

}
