<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class BasketRepository
{
    private Order $orderModel;
    private OrderItem $itemModel;
    private Product $productModel;
    private $key;

    public function __construct()
    {
        $this->key = request()->cookie('session_key');
        $this->orderModel = new Order;
        $this->itemModel = new OrderItem;
        $this->productModel = new Product;
    }

    // добавить позицию в item
    public function setItem(int $product_id, int $count, int $sum, int $sum_sale, array $detail)
    {
        $product = $this->productModel->findOrFail($product_id);
        return $this->itemModel->create([
            'session_key' => $this->key,
            'product_id' => $product_id,
            'name' => $product->name . ' (' . $product->art .')',
            'count' => $count,
            'sum' => $sum,
            'sum_sale' => $sum_sale,
            'detail' => $detail
        ]);
    }

    public function destroyItem($item_id)
    {
        return $this->itemModel::find($item_id)->delete();
    }

    public function addCount($item_id)
    {
        return $this->itemModel::find($item_id)->increment('count');
    }

    public function remCount($item_id)
    {
        return $this->itemModel::find($item_id)->decrement('count');
    }

    public function getBasket($select = [])
    {
        $query = $this->itemModel::whereNull('order_id')
            ->with(['product' => function ($q) {
                $q->select(['id', 'name', 'art', 'img']);
            }])
            ->whereSessionKey($this->key);
        if ($select)
            $query->select(array_merge($select, ['product_id']));
        return $query->get();
    }

    public function createOrder(array $data)
    {
        $exception = DB::transaction(function() use ($data) {

            $data['session_key'] = $this->key;
            $order = new $this->orderModel;
            $order->fill($data);
            $order->save();
            $order = $order->fresh();

            $this->itemModel::whereNull('order_id')
                ->whereSessionKey($this->key)
                ->update([ 'order_id' => $order->id ]);

        });

        if ($exception)
            throw new \Exception($exception);

        return true;
    }

}
