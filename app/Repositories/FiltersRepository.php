<?php

namespace App\Repositories;

use App\Models\Category;

class FiltersRepository
{
    private Category $category;

    public function __construct()
    {
        $this->category = new Category();
    }

    public function getFilters()
    {
        return $this->category::with('filters.items')
            ->with(['childrens' => function ($query) {
                $query->select(['id', 'name', 'slug', 'sort', 'parent_id']);
            }])
            ->select(['id', 'name', 'slug', 'sort'])
            ->whereNull('parent_id')
            ->whereFilterShow(true)
            ->get();
    }
}
