<?php

namespace App\Traits;

trait NumberFormatTrait
{
    public static function maney(int $number)
    {
        if ($number)
            return number_format($number, 0, ',', ' ').'р';
        return 'бесплатно';
    }

    public static function numberFormat(int $number)
    {

        return number_format($number, 0, ',', ' ');
    }
}
