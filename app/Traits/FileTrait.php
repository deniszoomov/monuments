<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait FileTrait
{
    public function putFile($path, $content)
    {
        Storage::disk('public')->put($path, $content);
    }

    public function deleteFile($path)
    {
        Storage::disk('public')->delete($path);
    }
}
