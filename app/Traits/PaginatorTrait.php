<?php

namespace App\Traits;

trait PaginatorTrait
{

    public static function getPaginator($paginate)
    {
        return [
            'total' => $paginate->total(),
            'currentPage' => $paginate->currentPage(),
            'currentUrl' => url()->current(),
            'lastPage' => $paginate->lastPage(),
            'hasPages' => $paginate->hasPages(),
        ];
    }

}
