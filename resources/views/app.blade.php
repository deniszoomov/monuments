@php
    try {
        $ssr = Http::post('http://localhost:8088/render', $page)->throw()->json();
    } catch (Exception $e) {
        $ssr = null;
    }
@endphp

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="color-scheme" content="light">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/guest.css') }}">

    <!-- Scripts -->
    @routes
    <script src="{{ mix('js/app.js') }}" defer></script>

    @foreach($ssr['head'] ?? [] as $element)
        {!! $element !!}
    @endforeach
</head>
<body class="min-h-screen bg-white">
    @if ($ssr)
        {!! $ssr['body'] !!}
    @else
        @inertia
    @endif
</body>
</html>
