<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require_once __DIR__.'/construkt/construkt-pam/PHPMailer/class.phpmailer.php';
require_once __DIR__.'/construkt/construkt-pam/PHPMailer/class.smtp.php';
$date = date("Y.m.d H:i",time());


$img = base64_decode($_POST['img']);
$filename = uniqid();
file_put_contents(__DIR__.'/construkt/construkt-pam/img/users-memorials/'.$filename.'.png', $img);
$filedir = __DIR__.'/construkt/construkt-pam/img/users-memorials/'.$filename.'.png';

$imy = $_POST['imy'];
$telefon = $_POST['telefon'];
$email = $_POST['email'];
$urlref = $_POST['urlref'];

// Настройки
$mail = new PHPMailer;

$mail->CharSet = 'UTF-8';
$mail->isSMTP();
$mail->Host = 'smtp.yandex.ru';
$mail->SMTPAuth = true;
$mail->Username = 'ваш логин'; // Ваш логин в Яндексе. Именно логин, без @yandex.ru. Эта почта только для транспортировки писем
$mail->Password = 'ваш пароль'; // Ваш пароль
$mail->SMTPSecure = 'ssl';
$mail->Port = 465;
$mail->setFrom('ваша почта@yandex.ru'); // Ваш Email
$mail->addAddress($_POST['email']); // Email получателя, это почта клиента, на неё идёт копия заявки
$mail->addAddress('ваша рабочая почта'); // Email получателя, это Ваша, сюда идут заявки
$mail->addAttachment($filedir); // Прикрепление файлов

// Письмо
$mail->isHTML(true);
$mail->Subject = "КОМПАНИЯ - Эскиз памятника"; // Заголовок письма, замените на название вашей организации
$mail->Body    = "<strong>Эскиз памятника:</strong><br>Файл: ".$filename."<br><br><strong>Заказчик:</strong><br>Имя: ".$imy."<br>Телефон: ".$telefon."<br>Эл.почта: ".$email."<br><br><br>ссылка на страницу сайта: ".$urlref; // Текст письма

// Результат
if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'ok';
}
