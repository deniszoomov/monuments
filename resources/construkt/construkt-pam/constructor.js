var srcImg = '/construkt/construkt-pam/img/constructor/';
// создаём "оболочку" вокруг canvas элемента (id="c")
var canvas = new fabric.Canvas('c');

canvas.setBackgroundImage('/construkt/construkt-pam/img/constructor/memorial/p1.png', canvas.renderAll.bind(canvas));

var HideControls = {
            'tl':true,
            'tr':false,
            'bl':true,
            'br':true,
            'ml':false,
            'mt':false,
            'mr':false,
            'mb':false,
            'mtr':true
        };

function addDeleteBtn(x, y){
    $(".deleteBtn").remove();
    var btnLeft = x-10;
    var btnTop = y-10;
    var deleteBtn = '<div class="deleteBtn" style="position:absolute;top:'+btnTop+'px;left:'+btnLeft+'px;cursor:pointer;width:20px;height:20px;">Х</div>';
    $(".canvas-container").append(deleteBtn);
}

canvas.on('object:selected',function(e){
        addDeleteBtn(e.target.oCoords.tr.x, e.target.oCoords.tr.y);
});



canvas.on('mouse:down',function(e){
    if(!canvas.getActiveObject())
    {
        $(".deleteBtn").remove();
    }
});

canvas.on('object:modified',function(e){
    addDeleteBtn(e.target.oCoords.tr.x, e.target.oCoords.tr.y);
});

canvas.on('object:scaling',function(e){
    $(".deleteBtn").remove();
});
canvas.on('object:moving',function(e){
    $(".deleteBtn").remove();
});
canvas.on('object:rotating',function(e){
    $(".deleteBtn").remove();
});
$(document).on('click',".deleteBtn",function(){
    if(canvas.getActiveObject())
    {
        canvas.remove(canvas.getActiveObject());
        $(".deleteBtn").remove();
    }
});

$(document).ready(function () {

    if($(window).width() < 240) {
        // change functionality for smaller screens
        $('#constructor .left').after($('#slide-mem'));
    }

    $('#slide-mem img').on('click', function() {
        var src = $(this).attr('src');
        canvas.setBackgroundImage(src, canvas.renderAll.bind(canvas));
    });


    $('.addImg img').on('click', function() {
        var src = $(this).attr('src');
        fabric.Image.fromURL(src, function(img) {
            img.top = 60;
            img.left = 30;
            img.scaleToWidth(100);
            img.scaleToHeight(100);
            img.setControlsVisibility(HideControls);
            canvas.add(img);
        });
        canvas.renderAll();
    });

    download_img = function(el) {
        canvas.discardActiveObject();
        $(".deleteBtn").remove();
        var image = canvas.toDataURL("image/png");
        el.href = image;
    };

    $('#printCanvas').on('click', function printCanvas() {

        const dataUrl = canvas.toDataURL();

        let windowContent = '<!DOCTYPE html>';
        windowContent += '<html>';
        windowContent += '<head><title>Эскиз памятника</title></head>';
        windowContent += '<body style="display:flex; justify-content: center;">';
        windowContent += '<div style="height: 550px; margin: 50px auto;">';
        windowContent += '<img style="max-height: 100%; max-width: 100%;" src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';

        const printWin = window.open('', '', 'width=' + screen.availWidth + ',height=700');
        printWin.document.open();
        printWin.document.write(windowContent);

        printWin.document.addEventListener('load', function() {
            printWin.focus();
            printWin.print();
            printWin.document.close();
            printWin.close();
        }, true);
    });

    $('#jsonCanvas').on('click', function () {
        $('#popup').css('display', 'block');
    });

    $('#popup .close').on('click', closePopup);

    function closePopup() {
        $('#popup').css('display', 'none');
        $('#popup .error').html('');
        $('#email').css('border', '1px solid #333');
        $('#email').val('');
    }

    $('#popup .button').on('click', sendImg);

    function sendImg() {
        var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var imgJson = canvas.toDataURL("image/png").replace("data:image/png;base64,", '');
        var email = $('#email').val();
        var imy = $('#imy').val();
        var telefon = $('#telefon').val();
        var urlref = $('#urlref').val();


        if(email != ''){
            if(email.search(pattern) == 0){
                $.ajax({
                    type: "post",
                    url: "save_image.php",
                    data: { img : imgJson, email : email, imy : imy, telefon : telefon, urlref : urlref },
                    success: function (data) {
                        console.log(data);
                    }
                });
                closePopup();
                alert('Эскиз отправлен на почту');

            }else{
                $('#popup .error').html('Введите корректный Email');
            }
        }else{
            $('#popup .error').html('Поле e-mail не должно быть пустым!');
            $('#email').css('border', '1px solid rgb(230, 87, 87)');
        }




    }

    //Аккордеон
    $(".accordeon").on('click', function () {
        $(this).siblings('ul').slideToggle('slow');
    });


 // Define an array with all fonts
var fonts = ['Roboto', 'Roboto Condensed', 'Roboto Slab', 'Poiret One', 'Playfair Display','Oswald','Open Sans Condensed','Open Sans','Marck Script','Lobster','Kelly Slab','Gabriela','EB Garamond','Cormorant Infant','Bad Script'];

WebFont.load({
    google: {
        families: fonts
    },
    active: function() {

        $('.addText li').on('click', function () {
            var selectFont = ($(this).data('font'));
            var text = ($(this).data('text'));
            addText(text, 100, selectFont);
        });


        addText('Ваш текст',400);
        canvas.add(textbox).setActiveObject(textbox).requestRenderAll();
        canvas.forEachObject (function (o) {
            if (o.type == 'text')
            {
            o._charWidthsCache = {};
            o._clearCache ();
            }
            });


        function addText (text, top, selectFont) {
            var textbox = new fabric.Textbox(text, {
                    left: 80,
                    top: top,
                    width: 150,
                    fontFamily: selectFont,
                    fill: '#fff'
                });
            canvas.add(textbox).setActiveObject(textbox);
        }

        // Add a Textbox using a custom font


        // Apply selected font on change
        document.getElementById('font-family').onchange = function() {
            canvas.getActiveObject().set("fontFamily", this.value);
            canvas.requestRenderAll();
        };

    },
});

    // });


});
