<html>
<head>

<title>Конструктор памятников на могилу</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="keywords" content="конструктор памятников, конструктор памятников на сайт, конструктор памятников для сайта, конструктор памятников онлайн, конструктор памятников сайт" />
<meta name="description" content="Контструктор памятников для сайта" />
<link href="../favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />

<!-- media queries -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>


<link rel="stylesheet" href="/construkt/construkt-pam/css/constructor.css">
<link rel="stylesheet" href="/construkt/construkt-pam/css/site_global.css">
<link rel="stylesheet" href="/construkt/construkt-pam/css/tabs.css">




<script src="/construkt/construkt-pam/fontfaceobserver.js"></script>


<script type="text/javascript" src="/construkt/construkt-pam/fabric/fabric.js"></script>


</head>
<body>
<div class="landing">
  <div class="clear"></div>
  <?php
$dirImg = $_SERVER['DOCUMENT_ROOT'].'/construkt/construkt-pam/img/constructor/';


function getImages($dir) {
    $nameImg = [];
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                if($file != '.' AND $file != '..') $nameImg[] = $file;
            }
            closedir($dh);
        }
    }
    return $nameImg;
}

$korpusImg = getImages($dirImg.'memorial/');
$korpusDvoynoyImg = getImages($dirImg.'memorial-dvoynoy/');
$korpusFigurnyImg = getImages($dirImg.'memorial-figurny/');
$korpusReznoyImg = getImages($dirImg.'memorial-reznoy/');
$korpusArkiImg = getImages($dirImg.'memorial-arki/');

?>
  <a href="/" >
  <div class="knopka-zakaz">Вернуться на сайт</div>
  </a>
  <div class="clear"></div>
  <h1>Конструктор памятников</h1>
  <div class="clear"></div>
  <div id="constructor">
    <div class="left">
      <div class="canvas-container" width="503" height="750"> <canvas id="c" width="503" height="750" src="" ></canvas>
      </div>
      <!-- Выводим типы памятников табами -->
      <section class="tabs-constructor">
      <input id="tab-constructor_1" type="radio" name="tab" checked="checked" />
      <input id="tab-constructor_2" type="radio" name="tab" />
      <input id="tab-constructor_3" type="radio" name="tab" />
      <input id="tab-constructor_4" type="radio" name="tab" />
      <input id="tab-constructor_5" type="radio" name="tab" />
      <label for="tab-constructor_1" id="tab-constructor_l1">
      <div class="tab-constructor-img"><img src="/construkt/construkt-pam/img/constructor/p-i-1.svg" alt="вертикальные памятники" title="вертикальные памятники"></div>
      Вертикальные</label> <label for="tab-constructor_2" id="tab-constructor_l2">
      <div class="tab-constructor-img"><img src="/construkt/construkt-pam/img/constructor/p-i-2.svg" alt="горизонтальные памятники" title="горизонтальные памятники"></div>
      Горизонтальные</label> <label for="tab-constructor_3" id="tab-constructor_l3">
      <div class="tab-constructor-img"><img src="/construkt/construkt-pam/img/constructor/p-i-3.svg" alt="фигурные памятники" title="фигурные памятники"></div>
      Фигурные</label> <label for="tab-constructor_4" id="tab-constructor_l4">
      <div class="tab-constructor-img"><img src="/construkt/construkt-pam/img/constructor/p-i-4.svg" alt="резные памятники" title="резные памятники"></div>
      Резные</label> <label for="tab-constructor_5" id="tab-constructor_l5">
      <div class="tab-constructor-img"><img src="/construkt/construkt-pam/img/constructor/p-i-5.svg" alt="памятники арки" title="памятники арки"></div>
      Арки</label>
      <div class="clear"></div>
      <div class="tabs-constructor_cont">
        <!-- Выводим 1-ый тип памятников во вкладке (в табе) -->
        <div id="tab-constructor_c1">
          <ul id="slide-mem">
            <?php
            asort($korpusImg);
            foreach ($korpusImg as $key => $val) {
            echo '<li><img src="/construkt/construkt-pam/img/constructor/memorial/'.$val.'"></li>';
            }
            ?>
          </ul>
        </div>
        <!-- Выводим 2-ый тип памятников во вкладке (в табе) -->
        <div id="tab-constructor_c2">
          <ul id="slide-mem">
            <?php
            asort($korpusDvoynoyImg);
            foreach ($korpusDvoynoyImg as $key => $val) {
            echo '<li><img src="/construkt/construkt-pam/img/constructor/memorial-dvoynoy/'.$val.'"></li>';
            }
            ?>
          </ul>
        </div>
        <!-- Выводим 3-ый тип памятников во вкладке (в табе) -->
        <div id="tab-constructor_c3">
          <ul id="slide-mem">
            <?php
            asort($korpusFigurnyImg);
            foreach ($korpusFigurnyImg as $key => $val) {
            echo '<li><img src="/construkt/construkt-pam/img/constructor/memorial-figurny/'.$val.'"></li>';
            }
            ?>
          </ul>
        </div>
        <!-- Выводим 4-ый тип памятников во вкладке (в табе) -->
        <div id="tab-constructor_c4">
          <ul id="slide-mem">
            <?php
             asort($korpusReznoyImg);
             foreach ($korpusReznoyImg as $key => $val) {
             echo '<li><img src="/construkt/construkt-pam/img/constructor/memorial-reznoy/'.$val.'"></li>';
             }
             ?>
          </ul>
        </div>
        <!-- Выводим 5-ый тип памятников во вкладке (в табе) -->
        <div id="tab-constructor_c5">
          <ul id="slide-mem">
            <?php
             asort($korpusArkiImg);
             foreach ($korpusArkiImg as $key => $val) {
             echo '<li><img src="/construkt/construkt-pam/img/constructor/memorial-arki/'.$val.'"></li>';
             }
             ?>
          </ul>
        </div>
      </div>
      </section> </div>
    <div class="right">
      <p style="margin: 10px 5px; padding: 0;">Выберите вариант портрета:</p>
      <div class="face-panel addImg">
        <div class="accordeon portret"> Гравировка</div>
        <ul class="hide">
          <?php
                $panelFace = getImages($dirImg.'face-panel/');
                asort($panelFace);
                foreach ($panelFace as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/face-panel/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon portret"> Фотокерамика</div>
        <ul class="hide">
          <?php
                $fotokeramika = getImages($dirImg.'fotokeramika/');
                asort($fotokeramika);
                foreach ($fotokeramika as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/fotokeramika/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon tablichki"> Табличка</div>
        <ul class="hide">
          <?php
                $tablichka = getImages($dirImg.'tablichki/');
                asort($tablichka);
                foreach ($tablichka as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/tablichki/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <p style="margin: 10px 5px; padding: 0;">Добавьте данные усопшего:</p>
      <div class="face-panel addText">
        <div class="accordeon"><img src="/construkt/construkt-pam/img/constructor/fio.svg" alt="">
          Фамилия</div>
        <ul class="hide colum-2" style="font-size: 21px; font-weight: bold;">
          <li style="font-family: 'Roboto Slab', serif;" data-font="Roboto Slab" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Roboto Condensed', sans-serif;" data-font="Roboto Condensed" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Roboto', sans-serif;" data-font="Roboto" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Poiret One', cursive;" data-font="Poiret One" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Playfair Display', serif;" data-font="Playfair Display" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Oswald', sans-serif;" data-font="Oswald" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Open Sans Condensed', sans-serif;" data-font="Open Sans Condensed" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Open Sans', sans-serif;" data-font="Open Sans" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Marck Script', cursive;" data-font="Marck Script" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Lobster', cursive;" data-font="Lobster" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Kelly Slab', cursive;" data-font="Kelly Slab" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Gabriela', serif;" data-font="Gabriela" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'EB Garamond', serif;" data-font="EB Garamond" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Cormorant Infant', serif;" data-font="Cormorant Infant" data-text="Фамилия">Фамилия</li>
          <li style="font-family: 'Bad Script', cursive;" data-font="Bad Script" data-text="Фамилия">Фамилия</li>
        </ul>
      </div>
      <div class="face-panel addText">
        <div class="accordeon"><img src="/construkt/construkt-pam/img/constructor/fio.svg" alt="">
          Имя</div>
        <ul class="hide colum-2" style="font-size: 21px; font-weight: bold;">
          <li style="font-family: 'Roboto Slab', serif;" data-font="Roboto Slab" data-text="Имя">Имя</li>
          <li style="font-family: 'Roboto Condensed', sans-serif;" data-font="Roboto Condensed" data-text="Имя">Имя</li>
          <li style="font-family: 'Roboto', sans-serif;" data-font="Roboto" data-text="Имя">Имя</li>
          <li style="font-family: 'Poiret One', cursive;" data-font="Poiret One" data-text="Имя">Имя</li>
          <li style="font-family: 'Playfair Display', serif;" data-font="Playfair Display" data-text="Имя">Имя</li>
          <li style="font-family: 'Oswald', sans-serif;" data-font="Oswald" data-text="Имя">Имя</li>
          <li style="font-family: 'Open Sans Condensed', sans-serif;" data-font="Open Sans Condensed" data-text="Имя">Имя</li>
          <li style="font-family: 'Open Sans', sans-serif;" data-font="Open Sans" data-text="Имя">Имя</li>
          <li style="font-family: 'Marck Script', cursive;" data-font="Marck Script" data-text="Имя">Имя</li>
          <li style="font-family: 'Lobster', cursive;" data-font="Lobster" data-text="Имя">Имя</li>
          <li style="font-family: 'Kelly Slab', cursive;" data-font="Kelly Slab" data-text="Имя">Имя</li>
          <li style="font-family: 'Gabriela', serif;" data-font="Gabriela" data-text="Имя">Имя</li>
          <li style="font-family: 'EB Garamond', serif;" data-font="EB Garamond" data-text="Имя">Имя</li>
          <li style="font-family: 'Cormorant Infant', serif;" data-font="Cormorant Infant" data-text="Имя">Имя</li>
          <li style="font-family: 'Bad Script', cursive;" data-font="Bad Script" data-text="Имя">Имя</li>
        </ul>
      </div>
      <div class="face-panel addText">
        <div class="accordeon"><img src="/construkt/construkt-pam/img/constructor/fio.svg" alt="">
          Отчество</div>
        <ul class="hide colum-2" style="font-size: 21px; font-weight: bold;">
          <li style="font-family: 'Roboto Slab', serif;" data-font="Roboto Slab" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Roboto Condensed', sans-serif;" data-font="Roboto Condensed" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Roboto', sans-serif;" data-font="Roboto" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Poiret One', cursive;" data-font="Poiret One" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Playfair Display', serif;" data-font="Playfair Display" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Oswald', sans-serif;" data-font="Oswald" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Open Sans Condensed', sans-serif;" data-font="Open Sans Condensed" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Open Sans', sans-serif;" data-font="Open Sans" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Marck Script', cursive;" data-font="Marck Script" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Lobster', cursive;" data-font="Lobster" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Kelly Slab', cursive;" data-font="Kelly Slab" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Gabriela', serif;" data-font="Gabriela" data-text="Отчество">Отчество</li>
          <li style="font-family: 'EB Garamond', serif;" data-font="EB Garamond" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Cormorant Infant', serif;" data-font="Cormorant Infant" data-text="Отчество">Отчество</li>
          <li style="font-family: 'Bad Script', cursive;" data-font="Bad Script" data-text="Отчество">Отчество</li>
        </ul>
      </div>
      <div class="face-panel addText">
        <div class="accordeon"><img src="/construkt/construkt-pam/img/constructor/calendar.svg" alt="">
          Даты</div>
        <ul class="hide colum-2" style="font-size: 21px; font-weight: bold;">
          <li style="font-family: 'Roboto Slab', serif;" data-font="Roboto Slab"                      data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Roboto Condensed', sans-serif;" data-font="Roboto Condensed"       data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Roboto', sans-serif;" data-font="Roboto"                           data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Poiret One', cursive;" data-font="Poiret One"                      data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Playfair Display', serif;" data-font="Playfair Display"            data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Oswald', sans-serif;" data-font="Oswald"                           data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Open Sans Condensed', sans-serif;" data-font="Open Sans Condensed" data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Open Sans', sans-serif;" data-font="Open Sans"                     data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Marck Script', cursive;" data-font="Marck Script"                  data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Lobster', cursive;" data-font="Lobster"                            data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Kelly Slab', cursive;" data-font="Kelly Slab"                      data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Gabriela', serif;" data-font="Gabriela"                            data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'EB Garamond', serif;" data-font="EB Garamond"                      data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Cormorant Infant', serif;" data-font="Cormorant Infant"            data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
          <li style="font-family: 'Bad Script', cursive;" data-font="Bad Script"                      data-text="23.04.1947 - 23.04.1947">23.04.1947</li>
        </ul>
      </div>
      <p style="margin: 10px 5px; padding: 0;">Подберите слова:</p>
      <div class="face-panel addImg">
        <div class="accordeon epitafiya"> Эпитафия</div>
        <ul class="hide colum-1">
          <?php
                $epitafii = getImages($dirImg.'epitafii/');
                asort($epitafii);
                foreach ($epitafii as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/epitafii/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon nadpis-izobrazhenie"> Надпись с изображением</div>
        <ul class="hide">
          <?php
                $nadpisizobrazhenie = getImages($dirImg.'nadpis-izobrazhenie/');
                asort($nadpisizobrazhenie);
                foreach ($nadpisizobrazhenie as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/nadpis-izobrazhenie/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <p style="margin: 10px 5px; padding: 0;">Добавьте оформления:</p>
      <div class="face-panel addImg">
        <div class="accordeon hram"> Церкви</div>
        <ul class="hide">
          <?php
                $hram = getImages($dirImg.'hram/');
                asort($hram);
                foreach ($hram as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/hram/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon ikona"> Иконы</div>
        <ul class="hide">
          <?php
                $ikona = getImages($dirImg.'ikona/');
                asort($ikona);
                foreach ($ikona as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/ikona/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon angel"> Ангелы</div>
        <ul class="hide">
          <?php
                $angel = getImages($dirImg.'angel/');
                asort($angel);
                foreach ($angel as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/angel/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon simvol"> Христианство</div>
        <ul class="hide">
          <?php
                $simvol = getImages($dirImg.'simvol/');
                asort($simvol);
                foreach ($simvol as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/simvol/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon svechi"> Свечи</div>
        <ul class="hide">
          <?php
                $candles = getImages($dirImg.'candles/');
                asort($candles);
                foreach ($candles as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/candles/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon krest"> Кресты</div>
        <ul class="hide">
          <?php
                $crosses = getImages($dirImg.'crosses/');
                asort($crosses);
                foreach ($crosses as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/crosses/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon vinetka"> Виньетки</div>
        <ul class="hide">
          <?php
                $vinetka = getImages($dirImg.'vinetki/');
                asort($vinetka);
                foreach ($vinetka as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/vinetki/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon ramka"> Рамки</div>
        <ul class="hide">
          <?php
                $ramka = getImages($dirImg.'ramka/');
                asort($ramka);
                foreach ($ramka as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/ramka/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon pticy"> Птицы</div>
        <ul class="hide">
          <?php
                $pticy = getImages($dirImg.'pticy/');
                asort($pticy);
                foreach ($pticy as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/pticy/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon vetv"> Ветви</div>
        <ul class="hide">
          <?php
                $vetki = getImages($dirImg.'vetki/');
                asort($vetki);
                foreach ($vetki as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/vetki/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon peyzazh"> Пейзажи</div>
        <ul class="hide">
          <?php
                $peyzazh = getImages($dirImg.'peyzazh/');
                asort($peyzazh);
                foreach ($peyzazh as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/peyzazh/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon cvetok"> Цветы</div>
        <ul class="hide">
          <?php
                $flowers = getImages($dirImg.'flowers/');
                asort($flowers);
                foreach ($flowers as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/flowers/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon uzor"> Узоры</div>
        <ul class="hide">
          <?php
                $uzor = getImages($dirImg.'uzor/');
                asort($uzor);
                foreach ($uzor as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/uzor/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon voin"> Военные</div>
        <ul class="hide">
          <?php
                $voin = getImages($dirImg.'voin/');
                asort($voin);
                foreach ($voin as $key => $val) {
                    echo '<li><img data-src="/construkt/construkt-pam/img/constructor/voin/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <p style="margin: 10px 5px; padding: 0;">Мусульманская тематика:</p>
      <div class="face-panel addImg">
        <div class="accordeon mecheti"> Мечети</div>
        <ul class="hide">
          <?php
                $mecheti = getImages($dirImg.'mecheti/');
                asort($mecheti);
                foreach ($mecheti as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/mecheti/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon msimvol"> Символика</div>
        <ul class="hide">
          <?php
                $msimvol = getImages($dirImg.'msimvol/');
                asort($msimvol);
                foreach ($msimvol as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/msimvol/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon epitafiya"> Надписи</div>
        <ul class="hide">
          <?php
                $mnadpisi = getImages($dirImg.'mnadpisi/');
                asort($mnadpisi);
                foreach ($mnadpisi as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/mnadpisi/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <div class="face-panel addImg">
        <div class="accordeon muzor"> Узоры</div>
        <ul class="hide">
          <?php
                $muzor = getImages($dirImg.'muzor/');
                asort($muzor);
                foreach ($muzor as $key => $val) {
                    echo '<li><img src="/construkt/construkt-pam/img/constructor/muzor/'.$val.'"></li>';
                }
            ?>
        </ul>
      </div>
      <p style="margin: 10px 5px; padding: 0;">Действия с эскизом:</p>
      <a id="download" download="memorial.png" href="" onclick="download_img(this);">
      <div class="button-download"><img src="/construkt/construkt-pam/img/constructor/download.svg" alt="" style="max-height: 20px;"><span>Скачать
        изображением</span></div>
      </a><br>
      <button class="button" id="printCanvas"><img src="/construkt/construkt-pam/img/constructor/print.svg" alt="">
      <span>Распечатать</span></button>
      <br>
      <br>
      <button class="button-zakaz" id="jsonCanvas"><img src="/construkt/construkt-pam/img/constructor/mail.svg" alt="">
      <span>Отправить на почту</span></button>
    </div>
  </div>
  <div class="clear"></div>
  <div class="pamyatnik-vnimanie"> Созданные макеты не хранятся на нашем сервере.
    При обновлении страницы, параметры будут сброшены к изначальным. Этот онлайн-конструктор
    предназначен для ознакомительных целей, пропорции памятников на могилу и декоративных
    элементов приблизительны. </div>
  <p>Данный конструктор позволяет оформить памятник на могилу в режиме онлайн
    и получить представление о сочетании различных элементов гравировки. В дальнейшем
    макет памятника, сделанный своими руками, можно сохранить, распечатать, отправить
    на почту или оставить заявку с эскизом для просчёта стоимости, не забудьте
    указать контакты для связи.</p>
  <div class="clear"></div>
  <br>
  <br>
  <p style="font-size: 13px; font-family: OpenSans-Regular, GothaProBol, Tahoma;"><a style="color: #000; text-decoration: none;" href="/construkt/construkt-pam/html/obrabotka.html" target="_blank">Соглашение
    на обработку персональных данных</a></p>
  <p style="font-size: 13px; font-family: OpenSans-Regular, GothaProBol, Tahoma;"><a style="color: #000; text-decoration: none;" href="/construkt/construkt-pam/html/cookie.htm" target="_blank">Политика
    в отношении файлов cookie</a></p>
  <p style="font-size: 13px; font-family: OpenSans-Regular, GothaProBol, Tahoma;"><a style="color: #000; text-decoration: none;" href="/construkt/construkt-pam/html/politika.html" target="_blank">Политика
    конфиденциальности</a></p>
  <br>
  <div class="clear"></div>
  <div class="clear"></div>
</div>
<div id="popup" style="display:none" class="formname"> <img src="/construkt/construkt-pam/img/constructor/close.svg" alt="" class="close">
  <h2>Отправить эскиз на почту</h2>
    <p class="error"></p>
    <input type="hidden" value="" id="urlref" name="urlref" />
    <p>Ваше Имя<br>
    <input placeholder="Представьтесь пожалуйста" type="text" name="imy" id="imy"></p>
    <p>Ваш Телефон<br>
    <input placeholder="+7 777 77 77 777" type="text" name="telefon" id="telefon"></p>
    <p>Ваш Email, копия эскиза памятника так же будет отправлена и на Вашу почту<br>
    <input type="text" name="" id="email"></p>

    <input checked="checked" id="checkbox" type="checkbox" name="checkbox" onchange="document.getElementById('submit').disabled = !this.checked;" />

  <label for="checkbox">Я ознакомлен(-а) с<br>
  Политикой конфиденциальности</label>

    <input class="button" type="submit" name="submit" id="submit" value="Отправить" />

</div>

<script>
document.getElementById("urlref").setAttribute("value",window.location.href);
</script>

<script src="/construkt/construkt-pam/constructor.js"></script>
<script src="/construkt/construkt-pam/jquery.lazyloadxt.js"></script>



<!-- Yandex.Metrika counter -->

<!-- /Yandex.Metrika counter -->

</body>
</html>
