import express from 'express'
import { createSSRApp, h } from 'vue'
import { renderToString } from '@vue/server-renderer'
import { createInertiaApp } from '@inertiajs/inertia-vue3'
import route from 'ziggy';

const server = express()
server.use(express.json())
server.post('/render', async (request, response, next) => {
    try {
        response.json(
            await createInertiaApp({
                title: title => `${title}`,
                page: request.body,
                render: renderToString,
                resolve: (name) => require(`./Pages/${name}`),
                setup({ app, props, plugin }) {

                    const Ziggy = {
                        ...request.body.props.ziggy,
                        location: new URL(request.body.props.ziggy.url)
                    }

                    return createSSRApp({
                        render: () => h(app, props),
                    }).mixin({
                        methods: {
                            route: (name, params, absolute, config = Ziggy) => route(name, params, absolute, config),
                        },
                    }).use(plugin)
                },
            })
        )
    } catch (error) {
        next(error)
    }
})
server.listen(8088, () => console.log('Server started.'))

console.log('Starting SSR server...')
