require('./bootstrap');
require('@fortawesome/fontawesome-free/js/all.js');

// Import modules...
import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';

import ElementPlus from 'element-plus'

const el = document.getElementById('app');

createInertiaApp({
    resolve: (name) => require(`./Pages/${name}`),
    setup({ el, app, props, plugin }) {
        createApp({ render: () => h(app, props) })
            .mixin({ methods: { route } })
            .use(plugin)
            .use(ElementPlus)
            .mount(el);
    },
});

InertiaProgress.init({ color: '#d2272f' });

